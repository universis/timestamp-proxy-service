const { ApplicationService, TraceUtils } = require('@themost/common');
// eslint-disable-next-line no-unused-vars
const { ExpressDataApplication } = require('@themost/express');
const proxy = require('express-http-proxy');
const url = require('url');
class TimestampServerProxy extends ApplicationService {
    constructor(app) {
        super(app);
        // get configuration
        this.options = {
            timestampServer: null,
            user: null,
            password: null
        }
        // get configuration
        const timestampProxyConfiguration = app.getConfiguration().getSourceAt('settings/universis/timestampProxy');
        if (timestampProxyConfiguration == null) {
            // throw error for invalid timestampProxy configuration
            // this operation is important to prevent invalid or missing configuration
            throw  new Error('Timestamp server configuration cannot be found or it is inaccessible. This is a critical application error. The operation cannot be continued.')
        }
        if (timestampProxyConfiguration.timestampServer == null) {
            throw  new Error('Timestamp server url cannot be empty at this context. This is a critical application error. The operation cannot be continued.')
        }
        Object.assign(this.options, timestampProxyConfiguration);
        // install service
        this.install();
    }

    install() {
        /**
         * @type {*|ExpressDataApplication}
         */
        const application = this.getApplication();
        application.container.subscribe( (container) => {
           if (container != null) {
                // create authorizationHeader
                let authorizationHeader = null;
                TraceUtils.info('TimestampServerProxy: Validate timestamp server service authorization.');
                if (this.options.user != null && this.options.password != null) {
                    authorizationHeader = "Basic " + new Buffer(`${this.options.user}:${this.options.password}`).toString('base64');
                    TraceUtils.info('TimestampServerProxy: Configuring timestamp server service authorization.');
                }
                else {
                    TraceUtils.info('TimestampServerProxy: Timestamp server service is being configured without authorization.');
                }
                const reqPath = url.parse(this.options.timestampServer).path;
                const reqServer = url.resolve(this.options.timestampServer, '/')
                container.use('/tsp', proxy(reqServer, {
                    parseReqBody: false,
                    proxyReqPathResolver: function() {
                        return reqPath;
                    },
                    proxyReqOptDecorator: function(proxyReqOpts, srcReq) {
                        return new Promise((resolve) => {
                            // set path
                            proxyReqOpts.path = reqPath;
                            if (authorizationHeader != null) {
                                proxyReqOpts.headers['authorization'] = authorizationHeader;
                            }
                            return resolve(proxyReqOpts);
                        });
                    }
                }));
           }
        });
    }

}

module.exports = {
    TimestampServerProxy
};