## timestamp-proxy

`timestamp-proxy` is a plugin of universis-api server

### Installation

    npm i https://gitlab.com/universis/timestamp-proxy-service.git

### Usage

Register `TimestampServerProxy` service under `services` section:

    {
     "services": [
            ...
            {
                "serviceType": "@universis/timestamp-proxy#TimestampServerProxy"
            },
            ...
    ]

Configure container application to use an external timestamp server which will accessible through

    https://<universis api server>/tsp

endpoint.

Set `timestampServer` URL to proxy request and optionally set credentials -`user` and `password`- if they are required.

    {
        "settings": {
            "universis": {
                "timestampProxy": {
                    "timestampServer": "https://example.com/TimeServer",
                    "user": "user",
                    "password": "password"
                }
            }
        }
    }